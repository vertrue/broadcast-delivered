import std.stdio;
import std.uuid;
import std.string;
import std.conv;
import core.time;
import vibe.core.core;
import vibe.data.json;
import vibe.http.server;
import vibe.http.router;
import vibe.http.client;
import std.json;

void main()
{
	auto router = new URLRouter;
	router
		.post("/getDeliveries",&contact);
	auto l = listenHTTP("0.0.0.0:8234", router);
	runApplication();
	scope (exit) {
	    exitEventLoop(true);
	    sleep(1.msecs);
    }
    l.stopListening();
}

void contact(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	auto broadcasts = req_end.json["broadcasts"];
	string key = req_end.json["apiKey"].get!string;
	long delivered = 0;
	for(int i = 0; i < broadcasts.length; i++)
		requestHTTP("https://graph.facebook.com/v4.0/" ~ broadcasts[i]["broadcast_id"].to!string ~ "/insights/messages_sent?access_token=" ~ key,					
			(scope req) {
				req.method = HTTPMethod.GET;
			},
			(scope res){
				Json ans = res.readJson;
				if(ans["error"].type == Json.Type.undefined)
					delivered += ans["data"][0]["values"][0]["value"].to!long;
			}
		);
	res_end.writeJsonBody(["delivered": delivered]);
}